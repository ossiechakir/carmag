<?php
/**
 *
 * @package amc
 */
get_header();
?>
<div class="container">
	<div class="error-page">
		<div class="error-number">
			<h1>404</h1>
		</div>
		<div class="error-text">
			<h2>
				<?php esc_html_e( 'UPPS! Page not found!', 'amc' ); ?>
			</h2>
			<p>
				<?php esc_html_e( 'The page you are looking for no longer exists.', 'amc' ); ?>
			</p>
			<a class="amc button" href="<?php esc_url( home_url( '/' ) ); ?>">
				<?php esc_html_e( 'GO TO HOMEPAGE', 'amc' ); ?>
			</a>
		</div>
	</div>
</div>
<?php
get_footer();
