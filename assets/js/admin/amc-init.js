(function($){
"use strict";
	jQuery(document).ready(function($){

		$("div[id^=amc-blogmeta-]").hide();

		var selectedbox = $('#post-formats-select input:checked').attr('value');
		$("#amc-blogmeta-" + selectedbox).stop(true,true).fadeIn(500);

	    $("#post-formats-select input[type=radio]").change(function(){
	        var diValue = $(this).attr("value");
	        $("div[id^=amc-blogmeta-]").hide();
	        $("#amc-blogmeta-" + diValue).fadeIn(1000);
	    });

	});

	$(window).load(function(){
	    $("#elementor-panel-categories").each(function(){
	    	$(this).find('.elementor-active').not('[id="elementor-panel-category-amc-shortcodes"]').children('.elementor-panel-category-items').css('display','none');
	    	$(this).find('.elementor-active').not('[id="elementor-panel-category-amc-shortcodes"]').removeClass('elementor-active');
	    });

	    $('.elementor-header-button').on('click', function(){
	    	setTimeout(function(){
	    		$('#elementor-panel-categories').find('.elementor-active').not('[id="elementor-panel-category-amc-shortcodes"]').children('.elementor-panel-category-items').css('display','none');
	    		$('#elementor-panel-categories').find('.elementor-active').not('[id="elementor-panel-category-amc-shortcodes"]').removeClass('elementor-active');
	    	}, 10);
	    });

	});
})(jQuery);