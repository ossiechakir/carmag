(function($) {
	"use strict";

	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (
				isMobile.Android() ||
				isMobile.BlackBerry() ||
				isMobile.iOS() ||
				isMobile.Opera() ||
				isMobile.Windows()
			);
		}
	};

	function equalHeightDiv(groupHeight) {
		groupHeight.each(function() {
			var tallest = 0;
			$(this)
				.find(".equal")
				.each(function() {
					var thisHeight = $(this).height();
					if (thisHeight > tallest) {
						tallest = thisHeight;
					}
				});
			$(this)
				.find(".equal")
				.height(tallest);
		});
	}

	$.fn.styleIsInViewport = function() {
		var elementTop = $(this).offset().top;
		var elementBottom = elementTop + $(this).outerHeight();
		var viewportTop = $(window).scrollTop();
		var viewportBottom = viewportTop + $(window).height();
		return elementBottom > viewportTop && elementTop < viewportBottom;
	};

	$(document).ready(function() {
		$(".follow-link-a").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
		});
		$(".follow-link").on("hover", function(e) {
			$(".follow-content").addClass("show");
		});

		$(document).on("click", function(event) {
			if (!$(event.target).closest(".follow-content").length) {
				$(".follow-content").removeClass("show");
			}
		});

		// var owl = $(".owl-carousel");
		// if ($(window).width() > 1180) {
		// 	owl.on("initialize.owl.carousel", function(event) {
		// 		var singleTopSection = ($(window).width() - 1200) / 2;
		// 		$(".single-top").css("left", singleTopSection + "px");
		// 	});
		// 	$(window).on("resize", function() {
		// 		var singleTopSection = ($(window).width() - 1200) / 2;
		// 		$(".single-top").css("left", singleTopSection + "px");
		// 	});
		// 	owl.on("initialize.owl.carousel", function(event) {
		// 		var singleTopSection = ($(window).width() - 1200) / 2;
		// 		$(".owl-custom-next-nav").css("right", singleTopSection + 10 + "px");
		// 	});
		// 	$(window).on("resize", function() {
		// 		var singleTopSection = ($(window).width() - 1200) / 2;
		// 		$(".owl-custom-next-nav").css("right", singleTopSection + 10 + "px");
		// 	});
		// }

		// $(".owl-custom-next-nav").click(function() {
		// 	owl.trigger("next.owl.carousel");
		// });

		// $(".owl-custom-prev-nav").click(function() {
		// 	owl.trigger("prev.owl.carousel");
		// });

		// owl.owlCarousel({
		// 	items: 5,
		// 	margin: 10,
		// 	loop: true,
		// 	nav: false,
		// 	dots: false,
		// 	responsive: {
		// 		0: {
		// 			items: 1
		// 		},
		// 		768: {
		// 			items: 3
		// 		},
		// 		1000: {
		// 			items: 5
		// 		}
		// 	}
		// });

		// Slider

		$(".full-screen-slider").slick({
			dots: true,
			autoplay: true,
			autoplaySpeed: 6000,
			nextArrow: ".next-post-title"
		});

		// Add smooth scrolling to all links
		$("a.smooth, .comments-blog-post-top a").on("click", function(event) {
			if (this.hash !== "") {
				event.preventDefault();
				// Store hash
				var hash = this.hash;
				$("html, body").animate(
					{
						scrollTop: $(hash).offset().top
					},
					800,
					function() {
						window.location.hash = hash;
					}
				);
			} // End if
		});

		$("body").on("click", function() {
			$(".search-wrapper").removeClass("open");
			$(".search-wrapper a").removeClass("isOpenYes");
			$(".search-wrapper a").addClass("isOpenNo");
		});

		$(".mobile-menu-button").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(".mobile-menu-icon-wrapper").toggleClass("open");
			$(".mobile-menu-box").toggleClass("open");
		});

		$(".fancy-sidebar-button").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(this).toggleClass("open");
		});

		$(".search-wrapper a").on("click", function(e) {
			e.preventDefault();
			e.stopPropagation();
			if ($(this).hasClass("isOpenNo")) {
				$(".search-wrapper").addClass("open");
				$(".search-wrapper input").focus();
				$(".search-wrapper a").removeClass("isOpenNo");
				$(".search-wrapper a").addClass("isOpenYes");
			} else {
				$(".search-wrapper").removeClass("open");
				$(".search-wrapper a").removeClass("isOpenYes");
				$(".search-wrapper a").addClass("isOpenNo");
			}
		});

		$(".search-wrapper").click(function(e) {
			e.preventDefault();
			e.stopPropagation();
			$(".search-wrapper").addClass("open");
			$(".search-wrapper input").focus();
		});

		// Menu
		$(".sf-menu")
			.superfish({
				delay: 0
			})
			.supposition();
		// Menu

		$(".fitvids").fitVids();

		$(".comments-blog-post-top a").on("click", function(e) {
			$("ol.comment-list").slideToggle("300");
		});

		$(".sticky").theiaStickySidebar({
			additionalMarginTop: 60
		});

		/*
    // LazyLoad
    if($('.lazy-load-active').length){
        var amcLazyLoad = new LazyLoad({
            threshold: 300,
            elements_selector: ".lazyimage",
            throttle: 80,
            class_loading: 'image-loading',
            class_loaded: 'image-loaded',
            data_src: "original",
            show_while_loading: true
        });
    }
    // LazyLoad


    $('.scroll-to-top').on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    if($('.single-sticky-bar, .sticky-header, .scroll-to-top').length){
        var windowScrollTop;
        var scroll_next_point;
        var single_sticky;
        var lastScrollTop = 0;
        var taglastScrollTop = 0;
        var butScrollTop = 0;
        $(window).scroll(function(){
            windowScrollTop = $(window).scrollTop();
            if($('.sticky-header').length){
                var sctop = $(this).scrollTop();
                if(windowScrollTop > 300){
                    if(sctop > lastScrollTop){
                        $('.sticky-header').removeClass('sticky-header-open');
                    }else{
                        $('.sticky-header').addClass('sticky-header-open');
                    }
                    lastScrollTop = sctop;
                }else{
                    $('.sticky-header').removeClass('sticky-header-open');
                }
            }

            if($('.scroll-to-top').length){
                var buttop = $(this).scrollTop();
                if(windowScrollTop > 300){
                    if(buttop > butScrollTop){
                        $('.scroll-to-top').css('bottom', '-45px');
                    }else{
                        $('.scroll-to-top').css('bottom', '45px');
                    }
                    butScrollTop = buttop;
                }else{
                    $('.scroll-to-top').css('bottom', '-45px');
                }
            }

            if($('.single-sticky-bar').length){
                if(windowScrollTop > 300){
                    $('.single-sticky-bar').addClass('single-stick-now');
                }else{
                    $('.single-sticky-bar').removeClass('single-stick-now');
                }
            }
        });
    }

    $('.fancy-sidebar-content').slimScroll({
        height: 'auto',
        color: '#4c4c4c',
        wheelStep: 10
    });
*/
	});

	$(window).load(function() {
		if (!isMobile.any()) {
		}
	});
})(jQuery);
