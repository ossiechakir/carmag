<?php
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-post clearfix">
	<div class="comments clearfix">
		<ol class="comment-list clearfix">
			<?php
			wp_list_comments(
				array(
					'callback' => 'amc_comment',
				)
			);
			?>
		</ol>
		<?php if ( get_option( 'page_comments' ) && get_comment_pages_count() > 1 ) : ?>
		<div class="clearfix">
			<div class="nav-previous pull-left"><?php previous_comments_link( __( '&larr; Older Comments', 'amc' ) ); ?>
			</div>
			<div class="nav-next pull-right"><?php next_comments_link( __( 'Newer Comments &rarr;', 'amc' ) ); ?></div>
		</div>
		<?php endif; ?>
	</div>
</div>

<?php

if ( comments_open() && ! post_password_required() ) {

	if ( get_comments_number() < 1 ) {
		$comment_class = ' no-comment-form';
	}

	?>
<h2><?php esc_html_e( 'Write Comment', 'amc' ); ?></h2>

<div class="comments-post-form clearfix <?php echo esc_attr( $comment_class ); ?>">
	<?php if ( ! comments_open() && get_comments_number() ) : ?>
	<p class="no-comments"><?php esc_html__( 'Comments are closed.', 'amc' ); ?></p>
	<?php endif; ?>

	<?php if ( comments_open() ) : ?>
	<div class="comment-styles">
		<div id="respond-wrap">
			<?php
				$commenter     = wp_get_current_commenter();
				$req           = get_option( 'require_name_email' );
				$aria_req      = ( $req ? " aria-required='true'" : '' );
				$fields        = array(
					'author' => '<div class="comment-first-line"><div class="com-col"><p class="comment-form-author comment-input-p"><input id="author" name="author" type="text" placeholder="' . esc_html__( 'Name', 'amc' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p></div>',
					'email'  => '<div class="com-col"><p class="comment-form-email comment-input-p"><input id="email" name="email" type="text" placeholder="' . esc_html__( 'E-Mail', 'amc' ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div>',
					'url'    => '<div class="com-col com-col-none"><p class="comment-form-url comment-input-p"><input id="url" name="url" placeholder="' . esc_html__( 'Website', 'amc' ) . '" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p></div></div>',
				);
				$comments_args = array(
					'fields'              => apply_filters( 'comment_form_default_fields', $fields ),
					/* translators: 1: Log Out 2: Profile */
					'logged_in_as'        => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'amc' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ) ) . '</p>',
					'title_reply'         => '',
					'title_reply_to'      => esc_html__( 'Leave a reply', 'amc' ),
					'cancel_reply_link'   => esc_html__( 'Click here to cancel the reply', 'amc' ),
					'label_submit'        => esc_html__( 'Post comment', 'amc' ),
					/* translators: Log-in Link */
					'must_log_in'         => '<p class="must-log-in">' . sprintf( esc_html__( 'You must be <a href="%s">logged in</a> to post a comment.', 'amc' ), wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ) ) . '</p>',
					'comment_notes_after' => '',
					'label_submit'        => esc_html__( 'Submit Comment', 'amc' ),
					'comment_field'       => '<div><p class="comment-form-comment comment-input-p"><textarea id="comment" name="comment" placeholder="' . esc_html__( 'Comment', 'amc' ) . '" aria-required="true"></textarea></p></div>',
				);
				?>
			<?php comment_form( $comments_args ); ?>
		</div>
	</div>
</div>
		<?php
		endif;
}
