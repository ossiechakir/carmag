<?php
	global $amc_opt;
if ( empty( $amc_opt['side-menu-sidebar'] ) ) {
		$amc_opt['side-menu-sidebar'] = '';
}
?>

<footer>
	<?php
	if (
	is_active_sidebar( 'footer-1' ) ||
	is_active_sidebar( 'footer-2' ) ||
	is_active_sidebar( 'footer-3' ) ||
	is_active_sidebar( 'footer-4' )
	) {
		?>
	<div class="footer-top">
		<div class="container">
			<div class="footer-wrapper">
				<div class="footer-column column-1">
					<?php
					if ( is_active_sidebar( 'footer-1' ) ) :
						if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-1' ) ) :
						endif;
					endif;
					?>
				</div>
				<div class="footer-column column-2">
					<?php
					if ( is_active_sidebar( 'footer-2' ) ) :
						if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-2' ) ) :
						endif;
					endif;
					?>
				</div>
				<div class="footer-column column-3">
					<?php
					if ( is_active_sidebar( 'footer-3' ) ) :
						if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-3' ) ) :
						endif;
					endif;
					?>
				</div>
				<div class="footer-column column-4">
					<?php
					if ( is_active_sidebar( 'footer-4' ) ) :
						if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-4' ) ) :
						endif;
					endif;
					?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php global $amc_opt; ?>

	<div class="pre-footer">

		<div class="container">
			<div class="row vcenter">
				<?php $logo_url = $amc_opt['footer-logo']['url']; ?>

				<div class="col-lg-3">
					<?php if ( $logo_url ) : ?>
					<div class="footer-logo">
						<img src="<?php echo esc_url( $logo_url ); ?>">
					</div>
					<?php endif; ?>
				</div>

				<div class="col-lg-6">
					<nav id="footer-menu">
						<?php
						if ( has_nav_menu( 'footer-menu' ) ) {
								wp_nav_menu(
									array(
										'theme_location' => 'footer-menu',
										'menu_class'     => 'footer-menu',
									),
								);
						}
						?>
					</nav>
				</div>

				<div class="col-lg-3">
					<div class="follow-content">

						<?php if ( ! empty( $amc_opt['facebook-header'] ) ) : ?>
						<a href="<?php echo esc_url( $amc_opt['facebook-header'] ); ?>">
							<i class="mag-icon-facebook-brands"></i>
						</a>
						<?php endif; ?>

						<?php if ( ! empty( $amc_opt['twitter-header'] ) ) : ?>
						<a href="<?php echo esc_url( $amc_opt['twitter-header'] ); ?>">
							<i class="mag-icon-twitter"></i>
						</a>
						<?php endif; ?>

						<?php if ( ! empty( $amc_opt['instagram-header'] ) ) : ?>
						<a href="<?php echo esc_url( $amc_opt['instagram-header'] ); ?>">
							<i class="mag-icon-instagram"></i>
						</a>
						<?php endif; ?>

						<?php if ( ! empty( $amc_opt['youtube-header'] ) ) : ?>
						<a href="<?php echo esc_url( $amc_opt['youtube-header'] ); ?>">
							<i class="mag-icon-youtube"></i>
						</a>
						<?php endif; ?>

						<?php if ( ! empty( $amc_opt['pinterest-header'] ) ) : ?>
						<a href="<?php echo esc_url( $amc_opt['pinterest-header'] ); ?>">
							<i class="mag-icon-pinterest"></i>
						</a>
						<?php endif; ?>

						<?php if ( ! empty( $amc_opt['vimeo-header'] ) ) : ?>
						<a href="<?php echo esc_url( $amc_opt['vimeo-header'] ); ?>">
							<i class="mag-icon-vimeo"></i>
						</a>
						<?php endif; ?>

					</div> <!-- Follow Content Finish -->

				</div>
			</div>

		</div>

	</div>
</footer>

<?php if ( $amc_opt['side-menu'] == 1 ) : ?>
</div>
</div>
<?php if ( $amc_opt['side-menu-animation'] == 'slide' ) : ?>

<div id="fancy-slide" class="fancy-sidebar fancy-slide">
	<aside class="fancy-sidebar-content widget-area">
		<?php
			if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( $amc_opt['side-menu-sidebar'] ) ) :
				endif;
			?>
	</aside>
</div>

<?php
			endif;
			endif;
?>
</div>
<?php wp_footer(); ?>
</body>

</html>