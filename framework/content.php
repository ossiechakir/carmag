<?php
/**
 * Index Post content
 *
 * @since 1.0.0
 */
?>

<div class="home-article">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<span class="sticky-post">
			<?php
			if ( is_sticky() ) {
				esc_html_e( 'Sticky', 'amc' ); }
			?>
		</span>
		<div class="row">
			<?php if ( has_post_thumbnail() ) { ?>
			<div class="blogmagazine-article-thumb col-lg-7">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'amc-classic' ); ?>
				</a>
			</div><!-- .blogmagazine-article-thumb -->
			<?php } ?>
			<div class="blogmagazine-archive-post-content-wrapper 
			<?php
			if ( has_post_thumbnail() ) {
				?>
				 col-lg-5 
				<?php
			} else {
				?>
				 col-lg-12 no-thumbnail <?php } ?> ">
				<header class="entry-header">
					<div class="entry-category">
						<?php $categories = get_the_category( get_the_ID() ); ?>
						<a class="post-categories-link"
							href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>">
							<?php echo esc_attr( $categories[0]->cat_name ); ?> </a>
					</div>
					<?php

					$title = get_the_title();

					if ( $title != '' ) {
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					} else {
						?>
					<h2 class="entry-title"><a href="<?php echo esc_url( get_permalink() ); ?>"
							rel="bookmark"><?php echo esc_attr( get_the_date() ); ?></a></h2>
						<?php
					}
					?>
					<div class="entry-excerpt">
						<?php echo amc_excerpt( 17, get_the_excerpt() ); ?>
					</div>


				</header><!-- .entry-header -->
				<div class="main-entry-content">
					<?php
					if ( 'post' === get_post_type() ) :
						?>
					<div class="entry-meta">
						<span class="author vcard"><?php echo esc_html__( 'by', 'amc' ); ?> <a class="<?php the_permalink(); ?>"
								href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
								<?php echo esc_html( get_the_author() ); ?> </a></span>
						<span class="posted-on">
							<span class="date"><?php echo esc_attr( $post_date = get_the_date() ); ?></span>
						</span>
					</div><!-- .entry-meta -->
						<?php
					endif;
					?>

				</div><!-- .entry-content -->

			</div><!-- .blogmagazine-archive-post-content-wrapper -->
		</div> <!-- .row -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
