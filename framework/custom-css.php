<?php
function amc_custom_styles() {
	global $amc_opt;
	wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/assets/css/custom-style.css' );

	if ( ! empty( $amc_opt['first-color'] ) ) {
		$first_color = $amc_opt['first-color'];}
	if ( ! empty( $amc_opt['second-color'] ) ) {
		$second_color = $amc_opt['second-color'];}
	if ( ! empty( $amc_opt['third-color'] ) ) {
		$third_color = $amc_opt['third-color'];}
	if ( ! empty( $amc_opt['fourth-color'] ) ) {
		$fourth_color = $amc_opt['fourth-color'];}
	if ( ! empty( $amc_opt['background-color'] ) ) {
		$body_background_color = $amc_opt['background-color'];}
	if ( ! empty( $amc_opt['header-background-color'] ) ) {
		$header_background_color = $amc_opt['header-background-color'];}
	if ( ! empty( $amc_opt['secondary-font-url'] ) ) {
		$secondary_font_url = $amc_opt['secondary-font-url'];}
	if ( ! empty( $amc_opt['secondary-font-semi-url'] ) ) {
		$secondary_font_semi_url = $amc_opt['secondary-font-semi-url'];}
	if ( ! empty( $amc_opt['secondary-font-bold-url'] ) ) {
		$secondary_font_bold_url = $amc_opt['secondary-font-bold-url'];}

	$custom_css = '';

	if ( ! empty( $first_color ) ) {

		$custom_css .= ".footer-explore ul li:nth-child(4n+1):after, .single-post-text input[type=submit], .colored-list-wrapper .colored-right .colored-list-post-box:nth-child(1){
			background-color: {$first_color} !important;
		}";

		$custom_css .= ".sidebar-widget .widget-title-cover h5{
			border-top: 1px solid {$first_color} !important;
		}";
	}

	if ( ! empty( $second_color ) ) {

		$custom_css .= ".slide-content .post-cat a, cite, p.wp-caption-text a, .single-post-text p a{
			color: {$second_color} !important;
		}";

		$custom_css .= ".colored-list-wrapper .colored-right .colored-list-post-box:nth-child(2), .footer-explore ul li:nth-child(4n+2):after, .header-top:after, ul.mega-sub-menu:before{
			background-color: {$second_color} !important;
		}";

		$custom_css .= ".classic-list-big-post-box{
			border-bottom: 6px solid {$second_color} !important;
		}";
	}

	if ( ! empty( $third_color ) ) {
		$custom_css .= ".colored-list-wrapper .colored-right .colored-list-post-box:nth-child(3), .footer-explore ul li:nth-child(4n+3):after, {
			background-color: {$third_color} !important;
		}";
	}

	if ( ! empty( $fourth_color ) ) {
		$custom_css .= ".colored-list-wrapper .colored-right .colored-list-post-box:nth-child(4), .footer-explore ul li:nth-child(4n+4):after, {
			background-color: {$fourth_color} !important;
		}";
	}

	if ( ! empty( $body_background_color ) ) {
		$custom_css .= "body{
			background-color: {$body_background_color} !important;
		}";
	}

	if ( $amc_opt['header-background-type'] == 'color' ) {
		if ( ! empty( $header_background_color ) ) {
			$custom_css .= ".header-top{
			background-color: {$header_background_color} !important;
		}";
		}
	} elseif ( $amc_opt['header-background-type'] == 'image' ) {
		$custom_css .= ".header-top{
			background: url('{$amc_opt['header-background-image']['url']}') center center / cover;
		}";
	}

	if ( $amc_opt['header-underline'] == 1 ) {
		$custom_css .= '.header-top:after{display:block !important;}';
	}

	wp_add_inline_style( 'custom-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'amc_custom_styles', 9999 );

