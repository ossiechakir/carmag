<?php
if ( ! class_exists( 'Redux' ) ) {
	//$amc_opt['main-color'] = "#BD081C";
} else {
	// This is your option name where all the Redux data is stored.
	$opt_name = 'amc_opt';

	$theme = wp_get_theme(); // For use with some settings. Not necessary.

	$args = array(
		// TYPICAL -> Change these values as you need/desire
		'opt_name'             => $opt_name,
		'disable_tracking'     => true,
		// This is where your data is stored in the database and also becomes your global variable name.
		'display_name'         => $theme->get( 'Name' ),
		// Name that appears at the top of your panel
		'display_version'      => $theme->get( 'Version' ),
		// Version that appears at the top of your panel
		'menu_type'            => 'menu',
		//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
		'allow_sub_menu'       => true,
		// Show the sections below the admin menu item or not
		'menu_title'           => esc_html__( 'amc Options', 'amc' ),
		'page_title'           => esc_html__( 'amc Options', 'amc' ),
		// You will need to generate a Google API key to use this feature.
		// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
		'google_api_key'       => 'AIzaSyC_DvFJA7SljYfSGUwT-N5VQWhz2iMK-RQ',
		// Set it you want google fonts to update weekly. A google_api_key value is required.
		'google_update_weekly' => false,
		// Must be defined to add google fonts to the typography module
		'async_typography'     => true,
		// Use a asynchronous font on the front end or font string
		//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
		'admin_bar'            => false,
		// Show the panel pages on the admin bar
		'admin_bar_icon'       => '',
		// Choose an icon for the admin bar menu
		'admin_bar_priority'   => 50,
		// Choose an priority for the admin bar menu
		'global_variable'      => '',
		// Set a different name for your global variable other than the opt_name
		'dev_mode'             => false,
		// Show the time the page took to load, etc
		'update_notice'        => true,
		// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
		'customizer'           => true,
		// Enable basic customizer support
		//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
		//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

		// OPTIONAL -> Give you extra features
		'page_priority'        => 3,
		// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
		'page_parent'          => 'themes.php',
		// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
		'page_permissions'     => 'manage_options',
		// Permissions needed to access the options panel.
		'menu_icon'            => AMC_IMAGES . '/redux-framework.png',
		// Specify a custom URL to an icon
		'last_tab'             => '',
		// Force your panel to always open to a specific tab (by id)
		'page_icon'            => 'icon-themes',
		// Icon displayed in the admin panel next to your menu_title
		'page_slug'            => '',
		// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
		'save_defaults'        => true,
		// On load save the defaults to DB before user clicks save or not
		'default_show'         => false,
		// If true, shows the default value next to each field that is not the default value.
		'default_mark'         => '',
		// What to print by the field's title if the value shown is default. Suggested: *
		'show_import_export'   => true,
		// Shows the Import/Export panel when not used as a field.

		// CAREFUL -> These options are for advanced use only
		'transient_time'       => 60 * MINUTE_IN_SECONDS,
		'output'               => true,
		// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
		'output_tag'           => true,
		// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
		// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
		'show_options_object'  => false,
		'show_import_export'   => true,
		// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
		'database'             => '',
		// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
		'system_info'          => false,
		// REMOVE

		//'compiler'             => true,

		// HINTS
		'hints'                => array(
			'icon'          => 'el el-question-sign',
			'icon_position' => 'right',
			'icon_color'    => 'lightgray',
			'icon_size'     => 'normal',
			'tip_style'     => array(
				'color'   => 'light',
				'shadow'  => true,
				'rounded' => false,
				'style'   => '',
			),
			'tip_position'  => array(
				'my' => 'top left',
				'at' => 'bottom right',
			),
			'tip_effect'    => array(
				'show' => array(
					'effect'   => 'slide',
					'duration' => '500',
					'event'    => 'mouseover',
				),
				'hide' => array(
					'effect'   => 'slide',
					'duration' => '500',
					'event'    => 'click mouseleave',
				),
			),
		),
	);

	// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.

	$args['share_icons'][] = array(
		'url'   => 'https://www.facebook.com/2035Themes',
		'title' => 'Like us on Facebook',
		'icon'  => 'el el-facebook',
	);
	$args['share_icons'][] = array(
		'url'   => 'https://twitter.com/2035Themes',
		'title' => 'Follow us on Twitter',
		'icon'  => 'el el-twitter',
	);

	Redux::setArgs( $opt_name, $args );

	Redux::setSection(
		$opt_name,
		array(
			'title'  => esc_html__( 'General', 'amc' ),
			'id'     => 'general',
			'icon'   => 'el el-home',
			'fields' => array(
				array(
					'id'       => 'first-color',
					'type'     => 'color',
					'title'    => esc_html__( 'First Color', 'amc' ),
					'subtitle' => esc_html__( 'Pick a main color for the theme (Default: #FF60A8).', 'amc' ),
					'default'  => '#001739',
				),
				array(
					'id'       => 'second-color',
					'type'     => 'color',
					'title'    => esc_html__( 'Second Color', 'amc' ),
					'subtitle' => esc_html__( 'Pick a main color for the theme (Default: #FFDD5C).', 'amc' ),
					'default'  => '#f8162e',
				),
				array(
					'id'       => 'third-color',
					'type'     => 'color',
					'title'    => esc_html__( 'Third Color', 'amc' ),
					'subtitle' => esc_html__( 'Pick a main color for the theme (Default: #3BEF93).', 'amc' ),
					'default'  => '#3BEF93',
				),
				array(
					'id'       => 'fourth-color',
					'type'     => 'color',
					'title'    => esc_html__( 'Fourth Color', 'amc' ),
					'subtitle' => esc_html__( 'Pick a main color for the theme (Default: #44C4ED).', 'amc' ),
					'default'  => '#44C4ED',
				),
				array(
					'id'       => 'background-color',
					'type'     => 'color',
					'title'    => esc_html__( 'Site Background Color', 'amc' ),
					'subtitle' => esc_html__( 'Pick a background color for the theme (Default: #F5F5F5).', 'amc' ),
					'default'  => '#FFF',
				),
				array(
					'id'      => 'lazy-load',
					'type'    => 'switch',
					'title'   => esc_html__( 'Lazy Load', 'amc' ),
					'default' => 0,
				),
				array(
					'id'      => 'scroll-top',
					'type'    => 'switch',
					'title'   => esc_html__( 'Scroll to Top Button', 'amc' ),
					'default' => 0,
				),
				array(
					'id'     => 'section-share',
					'type'   => 'section',
					'title'  => esc_html__( 'Share', 'amc' ),
					'indent' => true,
				),
				array(
					'id'      => 'share',
					'type'    => 'switch',
					'title'   => esc_html__( 'Share', 'amc' ),
					'default' => 1,
				),
				array(
					'id'       => 'share-facebook',
					'required' => array( 'share', 'equals', '1' ),
					'type'     => 'switch',
					'title'    => esc_html__( 'Facebook', 'amc' ),
					'default'  => 1,
				),
				array(
					'id'       => 'share-twitter',
					'required' => array( 'share', 'equals', '1' ),
					'type'     => 'switch',
					'title'    => esc_html__( 'Twitter', 'amc' ),
					'default'  => 1,
				),
				array(
					'id'       => 'share-pinterest',
					'required' => array( 'share', 'equals', '1' ),
					'type'     => 'switch',
					'title'    => esc_html__( 'Pinterest', 'amc' ),
					'default'  => 1,
				),
				array(
					'id'       => 'share-linkedin',
					'required' => array( 'share', 'equals', '1' ),
					'type'     => 'switch',
					'title'    => esc_html__( 'Linkedin', 'amc' ),
					'default'  => 0,
				),
			),
		)
	);

	Redux::setSection(
		$opt_name,
		array(
			'title'  => esc_html__( 'Typography', 'amc' ),
			'id'     => 'typography',
			'icon'   => 'el el-font',
			'fields' => array(
				array(
					'id'       => 'adobe-typekit-id',
					'type'     => 'text',
					'title'    => esc_html__( 'Adobe Typekit Kit ID', 'amc' ),
					'subtitle' => esc_html__( 'If you\'re using Adobe Typekit please write your Typekit Kit ID', 'amc' ),
				),
				array(
					'id'      => 'body-font-source',
					'type'    => 'select',
					'title'   => esc_html__( 'Body Font Source', 'amc' ),
					'options' => array(
						'google' => 'Google Fonts',
						'custom' => 'Custom Font',
						'adobe'  => 'Adobe Typekit',
					),
					'default' => 'google',
				),
				array(
					'id'             => 'body-font-google',
					'type'           => 'typography',
					'required'       => array( 'body-font-source', 'equals', 'google' ),
					'title'          => esc_html__( 'Body Font', 'amc' ),
					'font-backup'    => false,
					'font-size'      => false,
					'font-weight'    => true,
					'font-style'     => false,
					'text-align'     => false,
					'line-height'    => false,
					'color'          => false,
					'letter-spacing' => false,  // Defaults to false
					'subsets'        => true,
					'all_styles'     => true,
					'subtitle'       => esc_html__( 'Typography option with each property can be called individually.', 'amc' ),
					'default'        => array(
						'font-family' => 'Crimson Text',
						'google'      => true,
					),
				),
				array(
					'id'       => 'custom-body-name',
					'required' => array( 'body-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Body Font Name', 'amc' ),
					'subtitle' => esc_html__( 'Body Font Name', 'amc' ),
					'default'  => 'Libre Caslon Text',
				),
				array(
					'id'       => 'body-font-url',
					'required' => array( 'body-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Body Font', 'amc' ),
					'subtitle' => esc_html__( 'Body Font(Don\'t use filename extension)', 'amc' ),
					'default'  => AMC_THEMEROOT . '/assets/fonts/LibreCaslonText',
				),
				array(
					'id'      => 'secondary-font-source',
					'type'    => 'select',
					'title'   => esc_html__( 'Secondary Font Source', 'amc' ),
					'options' => array(
						'google' => 'Google Fonts',
						'custom' => 'Custom Font',
						'adobe'  => 'Adobe Typekit',
					),
					'default' => 'google',
				),
				array(
					'id'             => 'secondary-font-google',
					'type'           => 'typography',
					'required'       => array( 'secondary-font-source', 'equals', 'google' ),
					'title'          => esc_html__( 'Secondary Font', 'amc' ),
					'font-backup'    => false,
					'font-size'      => false,
					'font-weight'    => true,
					'font-style'     => false,
					'text-align'     => false,
					'line-height'    => false,
					'color'          => false,
					'letter-spacing' => false,  // Defaults to false
					'subsets'        => true,
					'all_styles'     => true,
					'subtitle'       => esc_html__( 'Typography option with each property can be called individually.', 'amc' ),
					'default'        => array(
						'font-family' => 'Poppins',
						'google'      => true,
					),
				),
				array(
					'id'       => 'custom-secondary-name',
					'required' => array( 'secondary-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Secondary Font Name', 'amc' ),
					'subtitle' => esc_html__( 'Secondary Font Name', 'amc' ),
					'default'  => '',
				),
				array(
					'id'       => 'secondary-font-url',
					'required' => array( 'secondary-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Secondary Font', 'amc' ),
					'subtitle' => esc_html__( 'Secondary Font(Don\'t use filename extension)', 'amc' ),
					'default'  => '',
				),
				array(
					'id'       => 'secondary-font-semi-url',
					'required' => array( 'secondary-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Secondary Font(Semi Bold)', 'amc' ),
					'subtitle' => esc_html__( 'Secondary Font(Don\'t use filename extension)', 'amc' ),
					'default'  => '',
				),
				array(
					'id'       => 'secondary-font-bold-url',
					'required' => array( 'secondary-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Secondary Font(Bold)', 'amc' ),
					'subtitle' => esc_html__( 'Secondary Font(Don\'t use filename extension)', 'amc' ),
					'default'  => '',
				),
				array(
					'id'      => 'heading-font-source',
					'type'    => 'select',
					'title'   => esc_html__( 'Heading Font Source', 'amc' ),
					'options' => array(
						'google' => 'Google Fonts',
						'custom' => 'Custom Font',
						'adobe'  => 'Adobe Typekit',
					),
					'default' => 'custom',
				),
				array(
					'id'             => 'heading-font-google',
					'type'           => 'typography',
					'required'       => array( 'heading-font-source', 'equals', 'google' ),
					'title'          => esc_html__( 'Heading Font', 'amc' ),
					'font-backup'    => false,
					'font-size'      => false,
					'font-weight'    => true,
					'font-style'     => false,
					'text-align'     => false,
					'line-height'    => false,
					'color'          => false,
					'letter-spacing' => false,  // Defaults to false
					'subsets'        => true,
					'all_styles'     => true,
					'subtitle'       => esc_html__( 'Typography option with each property can be called individually.', 'amc' ),
					'default'        => array(
						'font-family' => 'Crimson Text',
						'google'      => true,
					),
				),
				array(
					'id'       => 'custom-heading-name',
					'required' => array( 'heading-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Heading Font Name', 'amc' ),
					'subtitle' => esc_html__( 'Heading Font Name', 'amc' ),
					'default'  => 'Libre Caslon Text',
				),
				array(
					'id'       => 'heading-font-url',
					'required' => array( 'heading-font-source', 'equals', 'custom' ),
					'type'     => 'text',
					'title'    => esc_html__( 'Heading Font', 'amc' ),
					'subtitle' => esc_html__( 'Heading Font(Don\'t use filename extension)', 'amc' ),
					'default'  => AMC_THEMEROOT . '/assets/fonts/LibreCaslonText',
				),
				array(
					'id'       => 'heading-font-adobe',
					'type'     => 'text',
					'required' => array( 'heading-font-source', 'equals', 'adobe' ),
					'title'    => esc_html__( 'Typekit Font Name', 'amc' ),
					'subtitle' => esc_html__( 'Write your Typekit Font Name. Example: futura-pt', 'amc' ),
				),
			),
		)
	);

	Redux::setSection(
		$opt_name,
		array(
			'icon'   => 'el-icon-website',
			'title'  => esc_html__( 'Header', 'amc' ),
			'fields' => array(
				array(
					'id'      => 'header-style',
					'type'    => 'select',
					'title'   => esc_html__( 'Header Style', 'amc' ),
					'options' => array(
						'container' => 'Container',
						'full'      => 'Full Width',
					),
					'default' => 'full',
				),
				array(
					'id'      => 'header-layout',
					'type'    => 'image_select',
					'title'   => esc_html__( 'Header Layout', 'amc' ),
					'options' => array(
						'layout1' => array(
							'alt' => 'Layout 1',
							'img' => ReduxFramework::$_url . 'assets/img/1col.png',
						),
						'layout2' => array(
							'alt' => 'Layout 2',
							'img' => ReduxFramework::$_url . 'assets/img/2cl.png',
						),
					),
					'default' => 'layout1',
				),
				array(
					'id'       => 'logo-position',
					'type'     => 'select',
					'required' => array( 'header-layout', 'equals', 'layout1' ),
					'title'    => esc_html__( 'Logo Position', 'amc' ),
					'options'  => array(
						'left'   => 'Left',
						'center' => 'Center',
					),
					'default'  => 'left',
				),
				array(
					'id'      => 'header-background-type',
					'type'    => 'select',
					'title'   => esc_html__( 'Header Background Type', 'amc' ),
					'options' => array(
						'color' => 'Color',
						'image' => 'Image',
					),
					'default' => 'color',
				),
				array(
					'id'       => 'header-background-color',
					'required' => array( 'header-background-type', 'equals', 'color' ),
					'type'     => 'color',
					'title'    => esc_html__( 'Header Background Color', 'amc' ),
				),
				array(
					'id'       => 'header-background-image',
					'required' => array( 'header-background-type', 'equals', 'image' ),
					'type'     => 'media',
					'compiler' => 'true',
					'mode'     => false,
					'title'    => esc_html__( 'Header Background Image', 'amc' ),
					'subtitle' => esc_html__( 'Upload Background Image', 'amc' ),
				),
				array(
					'id'      => 'header-underline',
					'type'    => 'switch',
					'title'   => esc_html__( 'Header Underline', 'amc' ),
					'default' => 1,
				),
				array(
					'id'     => 'section-logo-start',
					'type'   => 'section',
					'title'  => esc_html__( 'Logo', 'amc' ),
					'indent' => true,
				),
				array(
					'id'       => 'logo',
					'type'     => 'media',
					'title'    => esc_html__( 'Logo', 'amc' ),
					'compiler' => 'true',
					'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
					'desc'     => esc_html__( 'Logo', 'amc' ),
					'subtitle' => esc_html__( 'Upload Logo', 'amc' ),
				),
				array(
					'id'       => 'logo-text',
					'type'     => 'text',
					'title'    => esc_html__( 'Logo Text', 'amc' ),
					'subtitle' => esc_html__( 'If you\'re not using image logo please write your logo text.', 'amc' ),
					'default'  => 'Turbomag',
				),
				array(
					'id'       => 'logo-height',
					'type'     => 'spinner',
					'title'    => esc_html__( 'Logo Height', 'amc' ),
					'subtitle' => esc_html__( 'Please select your logo height(Unit: px)', 'amc' ),
					'default'  => '35',
					'min'      => '20',
					'step'     => '1',
					'max'      => '500',
				),
				array(
					'id'     => 'section-menu-layout-start',
					'type'   => 'section',
					'title'  => esc_html__( 'Layout Details', 'amc' ),
					'indent' => true,
				),
				array(
					'id'      => 'side-menu',
					'type'    => 'switch',
					'title'   => esc_html__( 'Sidebar Menu', 'amc' ),
					'default' => 0,
				),
				array(
					'id'       => 'side-menu-sidebar',
					'type'     => 'select',
					'required' => array( 'side-menu', 'equals', true ),
					'title'    => esc_html__( 'Sidebar Menu Name', 'amc' ),
					'data'     => 'sidebars',
					'default'  => 'sidebar-1',
				),
				array(
					'id'       => 'side-menu-animation',
					'type'     => 'select',
					'required' => array( 'side-menu', 'equals', true ),
					'title'    => esc_html__( 'Sidebar Menu Animation', 'amc' ),
					'options'  => array(
						'slide' => 'Slide',
						'push'  => 'Push',
					),
					'default'  => 'push',
				),
				array(
					'id'      => 'search',
					'type'    => 'switch',
					'title'   => esc_html__( 'Search', 'amc' ),
					'default' => 1,
				),
				array(
					'id'       => 'trending',
					'required' => array( 'header-layout', 'equals', 'layout1' ),
					'type'     => 'switch',
					'title'    => esc_html__( 'Trending Bar', 'amc' ),
					'default'  => 1,
				),
				array(
					'id'      => 'social-media',
					'type'    => 'switch',
					'title'   => esc_html__( 'Social Media(Follow Section)', 'amc' ),
					'default' => 1,
				),

			),
		)
	);



	Redux::setSection(
		$opt_name,
		array(
			'icon'   => 'el-icon-website',
			'title'  => esc_html__( 'Social Media', 'amc' ),
			'fields' => array(

				array(
					'id'       => 'facebook-header',
					'type'     => 'text',
					'title'    => esc_html__( 'Facebook', 'amc' ),
					'desc'     => esc_html__( 'Facebook URL', 'amc' ),
					'subtitle' => esc_html__( 'Facebook URL', 'amc' ),
					'default'  => 'https://facebook.com/2035themes',
				),
				array(
					'id'       => 'instagram-header',
					'type'     => 'text',
					'title'    => esc_html__( 'Instagram', 'amc' ),
					'desc'     => esc_html__( 'Instagram URL', 'amc' ),
					'subtitle' => esc_html__( 'Instagram URL', 'amc' ),
					'default'  => 'https://instagram.com/2035themes',
				),
				array(
					'id'       => 'twitter-header',
					'type'     => 'text',
					'title'    => esc_html__( 'Twitter', 'amc' ),
					'desc'     => esc_html__( 'Twitter URL', 'amc' ),
					'subtitle' => esc_html__( 'Twitter URL', 'amc' ),
					'default'  => 'https://twitter.com/2035themes',
				),
				array(
					'id'       => 'youtube-header',
					'type'     => 'text',
					'title'    => esc_html__( 'Youtube', 'amc' ),
					'desc'     => esc_html__( 'Youtube Username', 'amc' ),
					'subtitle' => esc_html__( 'Youtube Username', 'amc' ),
					'default'  => '',
				),
				array(
					'id'       => 'pinterest-header',
					'type'     => 'text',
					'title'    => esc_html__( 'Pinterest', 'amc' ),
					'desc'     => esc_html__( 'Pinterest URL', 'amc' ),
					'subtitle' => esc_html__( 'Pinterest URL', 'amc' ),
					'default'  => '',
				),

			),
		)
	);

	Redux::setSection(
		$opt_name,
		array(
			'icon'   => 'el-icon-website',
			'title'  => esc_html__( 'Footer', 'amc' ),
			'fields' => array(
				array(
					'id'       => 'footer-logo',
					'type'     => 'media',
					'title'    => esc_html__( 'Footer Logo', 'amc' ),
					'compiler' => 'true',
					'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
					'desc'     => esc_html__( 'Footer Logo', 'amc' ),
					'subtitle' => esc_html__( 'Upload Footer Logo', 'amc' ),
				),

			),
		)
	);






	Redux::setSection(
		$opt_name,
		array(
			'title'  => esc_html__( 'Post Settings', 'amc' ),
			'id'     => 'post_settings',
			'icon'   => 'el el-list-alt',
			'fields' => array(
				array(
					'id'      => 'single-article-type',
					'type'    => 'select',
					'title'   => esc_html__( 'Article Layout', 'amc' ),
					'options' => array(
						'layout1' => 'Layout 1(Classic)',
						'layout2' => 'Layout 2(Classic Title Top)',
						'layout3' => 'Layout 3(Full Width Featured Image Title Bottom)',
						'layout4' => 'Layout 4(Full Width Featured Image Title Top)',
						'layout5' => 'Layout 5(Half Width Featured Image)',
						'layout6' => 'Layout 6(Full Width Covered Featured Image)',
					),
					'default' => 'layout1',
				),
				array(
					'id'      => 'single-related-posts',
					'type'    => 'switch',
					'title'   => esc_html__( 'Related Posts', 'amc' ),
					'default' => 0,
				),
				array(
					'id'       => 'single-sidebar-position',
					'type'     => 'select',
					'title'    => esc_html__( 'Sidebar Position', 'amc' ),
					'subtitle' => esc_html__( 'Sidebar Position Layout', 'amc' ),
					'options'  => array(
						'right' => 'Right Sidebar',
						'left'  => 'Left Sidebar',
						'none'  => 'No Sidebar',
					),
					'default'  => 'right',
				),
				array(
					'id'       => 'single-sidebar',
					'type'     => 'select',
					'required' => array( 'single-sidebar-position', 'not', 'none' ),
					'title'    => esc_html__( 'Single Sidebar', 'amc' ),
					'data'     => 'sidebars',
					'default'  => 'sidebar-1',
				),
			),
		)
	);

	Redux::setSection(
		$opt_name,
		array(
			'title'  => esc_html__( 'Page Settings', 'amc' ),
			'id'     => 'page_settings',
			'icon'   => 'el el-list-alt',
			'fields' => array(
				array(
					'id'      => 'page-comment-visibility',
					'type'    => 'switch',
					'title'   => esc_html__( 'Page Comments', 'amc' ),
					'default' => 1,
				),
				array(
					'id'     => 'section-category-start',
					'type'   => 'section',
					'title'  => esc_html__( 'Category Page', 'amc' ),
					'indent' => true,
				),
				array(
					'id'      => 'category-featured',
					'type'    => 'switch',
					'title'   => esc_html__( 'Featured Posts', 'amc' ),
					'default' => 1,
				),
				array(
					'id'       => 'category-sidebar',
					'type'     => 'select',
					'title'    => esc_html__( 'Category Sidebar', 'amc' ),
					'subtitle' => esc_html__( 'You can select different sidebar for each category from category page.', 'amc' ),
					'data'     => 'sidebars',
					'default'  => 'sidebar-1',
				),
				array(
					'id'     => 'section-tag-start',
					'type'   => 'section',
					'title'  => esc_html__( 'Tag Page', 'amc' ),
					'indent' => true,
				),
				array(
					'id'      => 'tag-sidebar',
					'type'    => 'select',
					'title'   => esc_html__( 'Tag Sidebar', 'amc' ),
					'data'    => 'sidebars',
					'default' => 'sidebar-1',
				),
				array(
					'id'     => 'section-search-start',
					'type'   => 'section',
					'title'  => esc_html__( 'Search Page', 'amc' ),
					'indent' => true,
				),
				array(
					'id'      => 'search-sidebar',
					'type'    => 'select',
					'title'   => esc_html__( 'Search Sidebar', 'amc' ),
					'data'    => 'sidebars',
					'default' => 'sidebar-1',
				),
				array(
					'id'     => 'section-author-start',
					'type'   => 'section',
					'title'  => esc_html__( 'Author Page', 'amc' ),
					'indent' => true,
				),
				array(
					'id'      => 'author-sidebar',
					'type'    => 'select',
					'title'   => esc_html__( 'Author Sidebar', 'amc' ),
					'data'    => 'sidebars',
					'default' => 'sidebar-1',
				),
				array(
					'id'      => 'author-page',
					'type'    => 'select',
					'title'   => esc_html__( 'Author Listing Page', 'amc' ),
					'data'    => 'pages',
					'default' => '',
				),
			),
		)
	);





	/*
	* <--- END SECTIONS
	*/

	/**
	 * This is a test function that will let you see when the compiler hook occurs.
	 * It only runs if a field    set with compiler=>true is changed.
	 * */
	function compiler_action( $options, $css, $changed_values ) {
		echo '<h1>The compiler hook has run!</h1>';
		echo '<pre>';
		print_r( $changed_values ); // Values that have changed since the last save
		echo '</pre>';
		//print_r($options); //Option values
		//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
	}

	/**
	 * Custom function for the callback validation referenced above
	 * */
	if ( ! function_exists( 'redux_validate_callback_function' ) ) {
		function redux_validate_callback_function( $field, $value, $existing_value ) {
			$error   = false;
			$warning = false;

			//do your validation
			if ( $value == 1 ) {
				$error = true;
				$value = $existing_value;
			} elseif ( $value == 2 ) {
				$warning = true;
				$value   = $existing_value;
			}

			$return['value'] = $value;

			if ( $error == true ) {
				$return['error'] = $field;
				$field['msg']    = 'your custom error message';
			}

			if ( $warning == true ) {
				$return['warning'] = $field;
				$field['msg']      = 'your custom warning message';
			}

			return $return;
		}
	}

	/**
	 * Custom function for the callback referenced above
	 */
	if ( ! function_exists( 'redux_my_custom_field' ) ) {
		function redux_my_custom_field( $field, $value ) {
			print_r( $field );
			echo '<br/>';
			print_r( $value );
		}
	}

	/**
	 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
	 * Simply include this function in the child themes functions.php file.
	 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
	 * so you must use get_template_directory_uri() if you want to use any of the built in icons
	 * */
	function dynamic_section( $sections ) {
		$sections[] = array(
			'title'  => esc_html__( 'Section via hook', 'amc' ),
			'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'amc' ),
			'icon'   => 'el el-paper-clip',
			// Leave this as a blank section, no options just some intro text set above.
			'fields' => array(),
		);

		return $sections;
	}

	/**
	 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
	 * */
	function change_arguments( $args ) {
		//$args['dev_mode'] = true;

		return $args;
	}

	/**
	 * Filter hook for filtering the default value of any given field. Very useful in development mode.
	 * */
	function change_defaults( $defaults ) {
		$defaults['str_replace'] = 'Testing filter hook!';

		return $defaults;
	}


	$tracking = get_option( 'redux-framework-tracking' );
	if ( $tracking && ( ! is_array( $tracking ) || empty( $tracking['allow-tracking'] ) ) ) {
		$tracking                   = array();
		$tracking['dev_mode']       = false;
		$tracking['allow_tracking'] = 'no';
		$tracking['tour']           = 0;
		update_option( 'redux-framework-tracking', $tracking );
	}
}