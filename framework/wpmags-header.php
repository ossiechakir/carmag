<?php global $amc_opt;
if ( ! class_exists( 'amcFramework' ) ) {
	if ( empty( $amc_opt['search'] ) ) {
		$amc_opt['search'] = 1;}
	if ( empty( $amc_opt['side-menu'] ) ) {
		$amc_opt['side-menu'] = 0;}
	if ( empty( $amc_opt['header-layout'] ) ) {
		$amc_opt['header-layout'] = 'layout1';}
	if ( empty( $amc_opt['logo-position'] ) ) {
		$amc_opt['logo-position'] = 'left';}
	if ( empty( $amc_opt['social-media'] ) ) {
		$amc_opt['social-media'] = 0;}
}
?>
<header>
	<a href="#" class="mobile-menu-button">
		<span class="mobile-menu-icon-wrapper">
			<span class="mobile-menu-icon-bar"></span>
			<span class="mobile-menu-icon-bar"></span>
			<span class="mobile-menu-icon-bar"></span>
			<span class="mobile-menu-icon-bar"></span>
		</span>
	</a>
	<div class="mobile-menu-box">

	</div>
	<div class="mobile-header">

	</div>
	<div
		class="header-top
		<?php
		if ( $amc_opt['logo-position'] == 'center' ) {
			echo ' logo-center';
		}  if ( $amc_opt['header-layout'] == 'layout2' ) {
			echo ' header-big'; }
		?>
		">
		<?php if ( $amc_opt['header-layout'] == 'layout2' ) { ?>
		<div class="header-left-big">
			<?php if ( $amc_opt['social-media'] == 1 ) { ?>

			<?php } ?>
			<span><?php echo get_bloginfo( 'description' ); ?></span>
		</div>
		<?php } ?>
		<div class="header-left">
			<div class="logo">
				<?php
				if ( empty( $amc_opt['logo']['url'] ) ) {

					?>
						<h2><a
						href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_attr( get_bloginfo() ); ?></a></h2>
										 <?php
				} else {
					?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_attr( $amc_opt['logo']['url'] ); ?>"
						class="img-fluid" style="height:<?php echo esc_attr( $amc_opt['logo-height'] ); ?>px"
						alt="<?php esc_attr_e( 'logo', 'amc' ); ?>" /></a>
															 <?php
				}
				?>
			</div>
		</div>
		<?php if ( $amc_opt['header-layout'] == 'layout1' ) { ?>
		<div class="header-center menu-wrap">
			<nav id="main-menu">
				<?php
				if ( has_nav_menu( 'main-menu' ) ) {
					wp_nav_menu(
						array(
							'theme_location' => 'main-menu',
							'menu_class'     => 'mini-menu sf-menu',
							'menu_id'        => 'navmain',
						)
					);
				}
				?>
			</nav>
		</div>
		<?php } ?>
		<div class="header-right">

			<?php if ( $amc_opt['social-media'] == 1 ) { ?>
			<div class="header-follow">
				<span class="follow-link">
					<a class="follow-link-a" href="#"><?php esc_html_e( 'Follow', 'amc' ); ?></a>
					<span class="follow-content">
						<?php
						if ( ! empty( $amc_opt['facebook-header'] ) ) {
							?>
							<a
							href="<?php echo esc_url( $amc_opt['facebook-header'] ); ?>"><i
								class="mag-icon-facebook-brands"></i></a><?php } ?>
						<?php
						if ( ! empty( $amc_opt['twitter-header'] ) ) {
							?>
							<a
							href="<?php echo esc_url( $amc_opt['twitter-header'] ); ?>"><i
								class="mag-icon-twitter"></i></a><?php } ?>
						<?php
						if ( ! empty( $amc_opt['instagram-header'] ) ) {
							?>
							<a
							href="<?php echo esc_url( $amc_opt['instagram-header'] ); ?>"><i
								class="mag-icon-instagram"></i></a><?php } ?>
						<?php
						if ( ! empty( $amc_opt['youtube-header'] ) ) {
							?>
							<a
							href="<?php echo esc_url( $amc_opt['youtube-header'] ); ?>"><i
								class="mag-icon-youtube"></i></a><?php } ?>
						<?php
						if ( ! empty( $amc_opt['pinterest-header'] ) ) {
							?>
							<a
							href="<?php echo esc_url( $amc_opt['pinterest-header'] ); ?>"><i
								class="mag-icon-pinterest"></i></a><?php } ?>
						<?php
						if ( ! empty( $amc_opt['vimeo-header'] ) ) {
							?>
							<a
							href="<?php echo esc_url( $amc_opt['vimeo-header'] ); ?>"><i class="mag-icon-vimeo"></i></a><?php } ?>
					</span>
				</span>
			</div>
			<?php } ?>

			<?php if ( $amc_opt['search'] == 1 ) { ?>
			<div id="search-wrapper" class="search-wrapper">
				<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
					<input type="search" id="topSearchForm" class="search-input"
						placeholder="<?php echo esc_attr__( 'Search', 'amc' ); ?>" required />
				</form>
				<a href="#" class="isOpenNo"><i class="mag-icon-search"></i></a>
			</div>
			<?php } ?>
			<?php
			if ( $amc_opt['side-menu'] == 1 ) {
				?>
				<a
				href="#
				<?php
				if ( $amc_opt['side-menu-animation'] == 'slide' ) {
					echo 'fancy-slide';
				} else {
					echo 'fancy-push'; }
				?>
				"
				class="fancy-sidebar-button"
				data-sidebar-effect="
				<?php
				if ( $amc_opt['side-menu-animation'] == 'slide' ) {
					echo 'fancy-slide';
				} else {
					echo 'fancy-push'; }
				?>
				"><span></span><span></span></a><?php } ?>
		</div>
	</div>
	<?php if ( $amc_opt['header-layout'] == 'layout2' ) { ?>
	<div class="layout2-menu-bar">
		<nav id="main-menu">
			<?php
			if ( has_nav_menu( 'main-menu' ) ) {
				wp_nav_menu(
					array(
						'theme_location' => 'main-menu',
						'menu_class'     => 'mini-menu',
						'menu_id'        => 'navmain',
					)
				);
			}
			?>
		</nav>
	</div>
	<?php } ?>
</header>










<div class='projects'>
	<div class='project'>s</div>
	<div class='project'>s</div>
	<div class='project'>s</div>
</div>
