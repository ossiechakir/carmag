<?php
/**
 *  amc functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package  amc
*/

/**************************************************************************************************/
/* Define Constants */
/**************************************************************************************************/
define( 'AMC_THEMEROOT', get_template_directory_uri() );
define( 'AMC_IMAGES', AMC_THEMEROOT . '/assets/images' );
define( 'AMC_THEME_VERSION', '1.0' );

/**************************************************************************************************/
/* Theme Setup  */
/**************************************************************************************************/

add_action( 'after_setup_theme', 'amc_setup' );
function amc_setup() {

	global $content_width;

	if ( ! isset( $content_width ) ) {
		$content_width = 1200;
	}

	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support(
		'post-formats',
		array(
			'audio',
			'image',
			'gallery',
			'video',
			'quote',
			'link',
		)
	);

	set_post_thumbnail_size( 180, 180, true );
	add_theme_support( 'custom-logo' );
	load_theme_textdomain( 'amc', get_template_directory() . '/framework/languages' );

	add_action( 'init', 'amc_register_menus' );

	// Add support for default block styles.
	add_theme_support( 'wp-block-styles' );

	// Editor  styles.
	add_theme_support( 'editor-styles' );
	add_editor_style( array( 'amc-gutenberg.css', amc_font_url() ) );

}

if ( ! function_exists( 'amc_font_url' ) ) :

	function amc_font_url() {

		$fonts_url = '';

		$poppins = _x( 'on', 'Poppins: on or off', 'amc' );

		if ( 'off' !== $poppins ) :

				$font_families = array();

				$font_families[] = 'Poppins::400,600,700,700italic';

				$query_args = array(
					'family' => urlencode( implode( '|', $font_families ) ),
					'subset' => urlencode( 'latin,latin-ext' ),
				);

				$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

		endif;

		return esc_url( $fonts_url );

	}

endif;


/* Admin Framework  */
if ( ! isset( $redux_demo ) && file_exists( get_template_directory() . '/framework/amc-config.php' ) ) {
	require_once( get_template_directory() . '/framework/amc-config.php' );
}

/* Register Menu */
function amc_register_menus() {
	register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'amc' ) ) );
	register_nav_menus( array( 'footer-menu' => esc_html__( 'Footer Menu', 'amc' ) ) );
	register_nav_menus( array( 'mobile-menu' => esc_html__( 'Mobile Menu', 'amc' ) ) );
}


/* Register Styles & Scripts  */
function amc_register_files() {

	// Styles

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', null, null );
	wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css', array(), '4.7.0' );
	wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', array(), '4.7.0' );
	wp_enqueue_style( 'amc-main', get_stylesheet_uri(), null, time() );

	// Scripts
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/slick.js', array( 'jquery' ) );
	wp_enqueue_script( 'priority-nav', get_template_directory_uri() . '/assets/js/priority-nav.js', array( 'jquery' ) );
	wp_enqueue_script( 'resizeListener', get_template_directory_uri() . '/assets/js/resizeListener.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-fancySidebar', get_template_directory_uri() . '/assets/js/fancy-sidebar.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-superfishpack', get_template_directory_uri() . '/assets/js/superfish.pack.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-req-libs', get_template_directory_uri() . '/assets/js/jquery.req-libs.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'amc-main', get_template_directory_uri() . '/assets/js/amc-main.js', array( 'jquery' ) );
}

add_action( 'wp_enqueue_scripts', 'amc_register_files' );

// Admin Styles & Scripts

if ( is_admin() ) :

	function amc_admin_files() {

		// Scripts
		wp_enqueue_script( 'amc-js', get_template_directory_uri() . '/assets/js/admin/amc-init.js', array( 'jquery' ) );
		wp_enqueue_media();
		wp_enqueue_script( 'amc-upload', get_template_directory_uri() . '/assets/js/admin/amc-upload-media.js', array( 'jquery' ) );

		// Styles
		wp_enqueue_style( 'amc-css', get_template_directory_uri() . '/assets/css/amc-admin.css' );

	}

endif;

add_action( 'admin_enqueue_scripts', 'amc_admin_files' );


/* Custom Widgets & Sidebars */

if ( ! function_exists( 'amc_register_sidebars' ) ) {

	function amc_register_sidebars() {

		if ( function_exists( 'register_sidebar' ) ) {

			// Right Sidebar
			register_sidebar(
				array(
					'name'          => esc_html__( 'Right Sidebar', 'amc' ),
					'id'            => 'right-sidebar',
					'description'   => esc_html__( 'Add widgets here to appear in your right sidebar.', 'amc' ),
					'before_widget' => '<div id="%1$s" class="amc-widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4 class="amc-title"><span class="title-wrapper">',
					'after_title'   => '</span></h4>',
				)
			);

			// Footer Widgets

			register_sidebar(
				array(
					'name'          => esc_html__( 'First Footer Column', 'amc' ),
					'id'            => 'footer-1',
					'description'   => esc_html__( 'This sidebar for footers first column.', 'amc' ),
					'before_widget' => '<div id="%1$s" class="amc-widget %2$s footer-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h5 class="amc-title"><span class="title-wrapper">',
					'after_title'   => '</span></h5>',
				)
			);

			register_sidebar(
				array(
					'name'          => esc_html__( 'Second Footer Column', 'amc' ),
					'id'            => 'footer-2',
					'description'   => esc_html__( 'This sidebar for footers second column.', 'amc' ),
					'before_widget' => '<div id="%1$s" class="amc-widget %2$s footer-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h5 class="amc-title"><span class="title-wrapper">',
					'after_title'   => '</span></h5>',
				)
			);

			register_sidebar(
				array(
					'name'          => esc_html__( 'Third Footer Column', 'amc' ),
					'id'            => 'footer-3',
					'description'   => esc_html__( 'This sidebar for footers third column.', 'amc' ),
					'before_widget' => '<div id="%1$s" class="amc-widget %2$s footer-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h5 class="amc-title"><span class="title-wrapper">',
					'after_title'   => '</span></h5>',
				)
			);

			register_sidebar(
				array(
					'name'          => esc_html__( 'Fourth Footer Column', 'amc' ),
					'id'            => 'footer-4',
					'description'   => esc_html__( 'This sidebar for footers fourth column.', 'amc' ),
					'before_widget' => '<div id="%1$s" class="amc-widget %2$s footer-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h5 class="amc-title"><span class="title-wrapper">',
					'after_title'   => '</span></h5>',
				)
			);
		}
	}

	add_action( 'widgets_init', 'amc_register_sidebars' );

}


/* Required Files */

require_once get_template_directory() . '/framework/custom-css.php';


require_once get_template_directory() . '/framework/custom-css.php';


/**************************************************************************************************/
/* TGM plugin activation */
/**************************************************************************************************/

include( get_template_directory() . '/framework/tgm/tgm-config.php' );


/* Image Sizes */
if ( function_exists( 'add_image_size' ) ) :

	add_image_size( 'amc-single-featured', 900, 500, true );
	add_image_size( 'amc-single-featured-2x', 1800, 1000, true );
	add_image_size( 'amc-classic', 670, 375, true );
	add_image_size( 'amc-classic-2x', 1340, 750, true );
	add_image_size( 'amc-single-top', 100, 80, true );
	add_image_size( 'amc-single-top-2x', 100, 80, true );
	add_image_size( 'amc-slider', 1440, 1280, true );
	add_image_size( 'amc-slider-2x', 2880, 2560, true );
	add_image_size( 'amc-title-on-image', 545, 445, true );
	add_image_size( 'amc-title-on-image-2x', 1090, 890, true );
	add_image_size( 'amc-grid-with-featured-image-big', 790, 490, true );
	add_image_size( 'amc-grid-with-featured-image-big-2x', 1580, 980, true );
	add_image_size( 'amc-grid-with-featured-image', 380, 240, true );
	add_image_size( 'amc-grid-with-featured-image-2x', 760, 480, true );
	add_image_size( 'amc-grid-no-padding', 310, 290, true );
	add_image_size( 'amc-grid-no-padding-2x', 620, 580, true );
	add_image_size( 'amc-featured-post', 1200, 600, true );
	add_image_size( 'amc-featured-post-2x', 2400, 1200, true );

endif;

function amc_post_thumbnail( $post_id, $size = 'image', $class = '' ) {

	$thumbnail    = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size )[0];
	$thumbnail_2x = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size . '-2x' )[0];

		$image  = '<img src="' . $thumbnail . '"';
		$image .= ( $thumbnail_2x ? ' srcset="' : '' );
		$image .= ( $thumbnail_2x ? $thumbnail_2x . ' 2x' : '' );
		$image .= ( $thumbnail_2x ? ', ' : '' );
		$image .= ( $thumbnail_2x ? '"' : '' );
		$image .= ( $class ? ' class="' . esc_attr( $class ) . '"' : '' );
		$image .= ' sizes="auto">';

	return $image;

}

// Get Avatar
function amc_get_avatar_url( $author_id, $size ) {

	$get_avatar = get_avatar( $author_id, $size );
	preg_match( "/src='(.*?)'/i", $get_avatar, $matches );

	return ( $matches[1] );

}

// Comment Edit

function amc_comment_form_edit( $fields ) {

	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;

}

add_filter( 'comment_form_fields', 'amc_comment_form_edit' );


/* Custom Comments */
function amc_comment( $comment, $args, $depth ) {

	$GLOBALS['comment'] = $comment; ?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
	<div id="comment-<?php comment_ID(); ?>" class="clearfix">
		<div class="user-comment-box clearfix">
			<div class="li-comment-box">
				<?php
				$check_avatar = get_avatar( $comment );

				if ( $check_avatar ) {
					$avatar_class = 'with-userimage';
				} else {
					$avatar_class = '';
				}
				?>

				<!-- Comment Info User -->
				<div class="comment-info-user <?php echo esc_attr( $avatar_class ); ?>">

					<?php
					if ( $check_avatar ) :
						echo get_avatar( $comment, 85, null, null, array( 'class' => array( 'img-fluid' ) ) );
					endif;

					$post_date_details = get_comment_date( 'F j, Y g:i' );
					$post_date         = get_comment_date( 'M j' );

					?>

					<a class="element-date date" title="<?php echo esc_attr( $post_date_details ); ?>"
						href="<?php the_permalink(); ?>"><?php echo esc_attr( $post_date ); ?></a>

				</div>

				<?php
				if ( is_user_logged_in() ) :
					$user_login_class = 'cuser-in';
				else :
					$user_login_class = '';
				endif;
				?>

				<!-- Comment Area Box -->

				<div class="comment-area-box <?php echo esc_attr( $avatar_class ); ?>">
					<div class="comment-text  <?php echo esc_attr( $user_login_class ); ?>">

						<div class="author author-title">

							<h4>
								<?php
								/* Translators: Comment Author Link */
								printf( esc_html__( '%s', 'amc' ), get_comment_author_link() );
								?>
							</h4>
						</div>

						<div class="comment-text-box"><?php comment_text(); ?></div>

						<div class="comment-tools">
							<?php
							comment_reply_link(
								array_merge(
									$args,
									array(
										'depth'     => $depth,
										'max_depth' => $args['max_depth'],
									)
								)
							);
							edit_comment_link( __( 'Edit Comment', 'amc' ), '<p>', '</p>' );
							?>
						</div>

					</div>

					<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php esc_html_e( 'Your comment is awaiting moderation.', 'amc' ); ?></em>
					<?php endif; ?>

				</div>

			</div>
		</div>
	</div>
</li>
<?php
}

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}


// Excerpt
function amc_excerpt( $limit, $value ) {
	$excerpt = explode( ' ', $value, $limit );
	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode( ' ', $excerpt ) . '...';
	} else {
		$excerpt = implode( ' ', $excerpt );
	}
	$excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );
	return $excerpt;
}

function amc_characters_excerpt( $limit, $value ) {

	$value = preg_replace( ' ([.*?])', '', $value );
	$value = strip_shortcodes( $value );
	$value = strip_tags( $value );
	$value = substr( $value, 0, $limit );
	$value = substr( $value, 0, strripos( $value, ' ' ) );
	$value = trim( preg_replace( '/\s+/', ' ', $value ) );
	$value = $value . '...';
	return $value;

}

function amc_social_share_single_links( $post_title, $post_permalink ) {

	global $amc_opt;

	if ( $amc_opt['share'] != false ) {

		$share_elements = array( $amc_opt['share-twitter'], $amc_opt['share-facebook'], $amc_opt['share-pinterest'], $amc_opt['share-linkedin'] );

		switch ( count( array_filter( $share_elements ) ) ) {
			case '4':
				$count = 'four';
				break;
			case '3':
				$count = 'three';
				break;
			default:
				$count = '';
				break;
		}

		?>

<div class="single-footer-share <?php echo esc_attr( $count ); ?> ">
	<?php if ( $amc_opt['share-twitter'] ) { ?>
	<a class="twitter"
		href="https://twitter.com/share?text=<?php echo htmlspecialchars( urlencode( html_entity_decode( $post_title, ENT_COMPAT, 'UTF-8' ) ), ENT_COMPAT, 'UTF-8' ); ?>&url=<?php echo urlencode( esc_url( $post_permalink ) ); ?>"
		onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
		target="_blank" title="<?php esc_html_e( 'Share On Twitter', 'amc' ); ?>">
		<span class="share-icon"><i class="mag-icon-twitter"></i></span>
		<span class="share-text"><?php esc_html_e( 'Share On Twitter', 'amc' ); ?></span>
	</a>
	<?php } ?>

	<?php if ( $amc_opt['share-facebook'] ) { ?>
	<a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode( esc_url( $post_permalink ) ); ?>"
		onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
		target="_blank" title="<?php esc_html_e( 'Share On Facebook', 'amc' ); ?>">
		<span class="share-icon"><i class="mag-icon-facebook-brands"></i></span>
		<span class="share-text"><?php esc_html_e( 'Share On Facebook', 'amc' ); ?></span>
	</a>
	<?php } ?>

	<?php if ( $amc_opt['share-pinterest'] ) { ?>
	<a class="pinterest"
		href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode( esc_url( $post_permalink ) ); ?>&description=<?php echo htmlspecialchars( urlencode( html_entity_decode( $post_title, ENT_COMPAT, 'UTF-8' ) ), ENT_COMPAT, 'UTF-8' ); ?>&summary=&source="
		onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
		target="_blank" title="<?php esc_html_e( 'Share On Pinterest', 'amc' ); ?>">
		<span class="share-icon"><i class="mag-icon-pinterest"></i></span>
		<span class="share-text"> <?php esc_html_e( 'Share On Pinterest', 'amc' ); ?></span>
	</a>
	<?php } ?>

	<?php if ( $amc_opt['share-linkedin'] ) { ?>
	<a class="linkedin"
		href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode( esc_url( $post_permalink ) ); ?>&title=<?php echo htmlspecialchars( urlencode( html_entity_decode( $post_title, ENT_COMPAT, 'UTF-8' ) ), ENT_COMPAT, 'UTF-8' ); ?>"
		onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
		target="_blank" title="<?php esc_html_e( 'Share On Linkedin', 'amc' ); ?>">
		<span class="share-icon"><i class="mag-icon-linkedin"></i></span>
		<span class="share-text"><?php esc_html_e( 'Share On Linkedin', 'amc' ); ?></span>
	</a>
	<?php } ?>
</div>
<?php } ?>
<?php
}
add_action( 'amc_social_share_single', 'amc_social_share_single_links', 10, 25 );



function amc_is_elementor() {
	global $post;
	return \Elementor\Plugin::$instance->db->is_built_with_elementor( $post->ID );
}

function get_previous_post_id( $post_id ) {
	// Get a global post reference since get_adjacent_post() references it
	global $post;
	// Store the existing post object for later so we don't lose it
	$oldGlobal = $post;
	// Get the post object for the specified post and place it in the global variable
	$post = get_post( $post_id );
	// Get the post object for the previous post
	$previous_post = get_previous_post();
	// Reset our global object
	$post = $oldGlobal;
	if ( '' == $previous_post ) {
		return 0;
	}
	return $previous_post->ID;
}