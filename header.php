<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php global $amc_opt; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_site_icon(); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="wrapper" >
	<?php if ( 1 === $amc_opt['side-menu'] ) { ?>
	<div class="fancy-sidebar-pusher">
		<?php if ( 'push' === $amc_opt['side-menu-animation'] ) { ?>
	<div id="fancy-push" class="fancy-sidebar fancy-push">
		<aside class="fancy-sidebar-content widget-area">
			<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( $amc_opt['side-menu-sidebar'] ) ) : ?>
			<?php endif; ?>
		</aside>
	</div>
	<?php } ?>
	<div id="site-content" class="site-content">
	<?php } ?>
	<?php
	get_template_part( 'framework/amc-header' );

