<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amc
*/

get_header();
?>



<div class="container site-index-container">
	<div class="row">
		<div class="col-lg-9 col-sm-9 blog-index">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					get_template_part( 'framework/content' );
				endwhile; else :
					?>
			<div class="search-notice"><?php esc_html_e( 'Your search returned no results. Please try a different keyword!', 'amc' ); ?></div>
					<?php
				endif;
				the_posts_pagination(
					array(
						'mid_size'           => 3,
						'prev_text'          => __( 'Previous', 'amc' ),
						'next_text'          => __( 'Next', 'amc' ),
						'screen_reader_text' => ' ',
					)
				);
				wp_reset_postdata();
				?>
		</div>
		<aside id="secondary" class="col-lg-3 col-sm-3 sidebar-main widget-area sticky" role="complementary">
			<?php if ( is_active_sidebar( 'right-sidebar' ) ) { ?>
				<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'right-sidebar' ) ) : ?>
				<?php endif; ?>
			<?php } ?>
		</aside>
	</div>
</div>


<?php
get_footer();
?>


}
