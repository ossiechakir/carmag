<?php
get_header();
$elementor_page_check = ( amc_is_elementor() ? 'front-page' : '' );
?>
<div class="container single-page <?php echo esc_html( $elementor_page_check ); ?> ">
	<div class="row">
		<?php if ( ! is_front_page() ) { ?>
		<div class="col-lg-9 col-sm-9">
			<?php } else { ?>
			<div class="col-lg-12 col-sm-12">
				<?php } ?>
				<?php if ( ! is_front_page() ) { ?>
				<h1>
					<?php
					$title = get_the_title();
					if ( '' !== $title ) {
						echo esc_attr( $title );
					} else {
						$post_date = get_the_date( 'F j' );
						echo esc_attr( $post_date ); }
					?>
				</h1>
				<?php } ?>
				<div class="entry-content clearfix">
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) :
							the_post();
							the_content();
							wp_link_pages(
								array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'amc' ),
									'after'  => '</div>',
								)
							);
					endwhile;
endif;
					wp_reset_postdata();
					?>
				</div>
			</div>
			<?php if ( ! is_front_page() ) { ?>
			<aside id="secondary" class="col-lg-3 col-sm-3 sidebar-main widget-area sticky" role="complementary">
				<?php
				if ( is_active_sidebar( 'right-sidebar' ) ) {
					if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'right-sidebar' ) ) :
				endif;
					?>
				<?php } ?>
			</aside>
			<?php } ?>
		</div>
	</div>

	<?php
	get_footer();