<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package amc
 */
get_header();
global $amc_opt;
if ( have_posts() ) {
	the_post();
}

function amc_set_post_views( $post_id ) {
	$count_key = 'post_views_count';
	$count     = get_post_meta( $post_id, $count_key, true );
	if ( '' === $count ) {
		$count = 0;
		delete_post_meta( $post_id, $count_key );
		add_post_meta( $post_id, $count_key, '0' );
	} else {
		$count++;
		update_post_meta( $post_id, $count_key, $count );
	}
}

amc_set_post_views( get_the_ID() );

$post_title     = get_the_title();
$post_permalink = get_the_permalink();


?>
<div class="single-wrapper">


	<?php

			$recent_args        = array(
				'post_type'   => 'post',
				'post_status' => 'publish',
			);
			$blogmagazine_query = new WP_Query( $recent_args );

			if ( $blogmagazine_query->have_posts() && $blogmagazine_query->found_posts > 6 ) {
				?>

	<div class="single-top">
		<div class="owl-custom-next-nav">
			<svg width="6px" height="10px" viewBox="0 0 6 10" version="1.1" xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="Home-Copy-49" transform="translate(-1214.000000, -277.000000)" fill="#000000" fill-rule="nonzero">
						<g id="chevron-right-solid" transform="translate(1214.000000, 277.000000)">
							<path
								d="M5.68647212,5.19136211 L1.41090403,9.46690821 C1.20469803,9.67311421 0.870386019,9.67311421 0.664202014,9.46690821 L0.165528004,8.96823419 C-0.0403260009,8.76238019 -0.0407220009,8.42875018 0.164648004,8.22241218 L3.55311008,4.8180001 L0.164648004,1.41361003 C-0.0407220009,1.20727203 -0.0403260009,0.873642019 0.165528004,0.667788014 L0.664202014,0.169114004 C0.870408019,-0.0370920008 1.20472003,-0.0370920008 1.41090403,0.169114004 L5.68645012,4.4446601 C5.89265613,4.6508441 5.89265613,4.98515611 5.68647212,5.19136211 Z"
								id="Path"></path>
						</g>
					</g>
				</g>
			</svg>
		</div>
		<div class="owl-custom-prev-nav">
			<svg width="6px" height="10px" viewBox="0 0 6 10" version="1.1" xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="Home-Copy-49" transform="translate(-1273.000000, -279.000000)" fill="#000000" fill-rule="nonzero">
						<g id="chevron-left-solid" transform="translate(1273.000000, 279.000000)">
							<path
								d="M0.164312003,4.41435559 L4.41085959,0.168026503 C4.61559409,-0.0367080008 4.9477141,-0.0367080008 5.15244861,0.168026503 L5.64778812,0.663366014 C5.85230412,0.867882018 5.85252262,1.19912802 5.64866212,1.40408103 L2.28310655,4.7851501 L5.64844362,8.16643767 C5.85252262,8.37139067 5.85208562,8.70263668 5.64756962,8.90715268 L5.15223011,9.40249219 C4.9474956,9.6072267 4.61537559,9.6072267 4.41064109,9.40249219 L0.164312003,5.15594461 C-0.0404225008,4.9512101 -0.0404225008,4.61909009 0.164312003,4.41435559 Z"
								id="Path"></path>
						</g>
					</g>
				</g>
			</svg>
		</div>

		<div class="top-recent-post">
			<div class="owl-carousel clearfix">

				<?php

				while ( $blogmagazine_query->have_posts() ) {
					$blogmagazine_query->the_post();
					?>
				<div class="item">
					<div class="single-post-top-wrapper">
						<div class="single-top-post-thumb">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'amc-single-top' ); ?>
							</a>
						</div><!-- .single-top-post-thumb -->
						<div class="single-top-post-content">
							<h6>
								<a href="<?php the_permalink(); ?>">
									<?php
										echo esc_attr( amc_characters_excerpt( 22, get_the_title() ) );
									?>
								</a>
							</h6>
							<div class="single-top-post-meta">
								<span class="posted-on">
									<span class="date"><?php echo esc_attr( get_the_date() ); ?></span>
								</span>
							</div>
						</div><!-- .single-top-post-content -->
					</div><!-- .single-post-top-wrapper -->
				</div>
				<?php
				}

				wp_reset_postdata();
				?>
			</div><!-- .-->
		</div><!-- . -->
	</div>
	<?php } ?>




	<div class="single-container container">
		<div class="single-container-columns">
			<div class="row">
				<div class="col-lg-12">
					<header class="entry-header">
						<?php
						if ( has_category() ) {
							?>
						<div class="entry-category">
							<h6><?php the_category( ' / ' ); ?></h6>
						</div><?php } ?>
						<h1 class="entry-title">
							<?php
							$title = get_the_title(); if ( '' !== $title ) {
								$allowed_tag = array(
									'i'  => '',
									'br' => '',
								);
								echo wp_kses( $title, $allowed_tag );
							} else {
								echo esc_html( get_the_date( 'F j' ) );
							}
							?>
						</h1>
						<div class="entry-meta">
							<span class="author vcard"><a class="<?php the_permalink(); ?>"
									href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
									<?php echo esc_html__( 'by', 'amc' ) . ' ' . esc_html( get_the_author() ); ?> </a></span>
							<span class="posted-on">
								<span class="date"><?php echo esc_attr( get_the_date() ); ?></span>
							</span>
						</div><!-- .entry-meta -->
					</header>
				</div>

				<div class="single-main-area col-lg-9 col-sm-9">



					<?php
					if ( has_post_thumbnail() ) {
						echo '<div class="featured-image">' . amc_post_thumbnail( $post->ID, 'amc-single-featured' ) . '</div>';
					}
					?>
					<div class="single-post-text-wrapper fitvids">
						<?php if ( class_exists( 'amcFramework' ) ) { ?>
						<div class="single-post-share">
							<?php do_action( 'amc_social_share_single', $post_title, $post_permalink ); ?>
						</div>
						<?php } ?>
						<div class="post-content">
							<div class="entry-content clearfix">
								<?php
								the_content();
								wp_link_pages( 'before=<div class="post-paginate">&after=</div>' );
								?>
							</div>
							<footer class="entry-footer">
								<div class="finish-flag">
									<svg width="14px" height="16px" viewBox="0 0 14 16" version="1.1" xmlns="http://www.w3.org/2000/svg"
										xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g transform="translate(-273.000000, -4597.000000)" fill="#F83146" fill-rule="nonzero">
												<g transform="translate(273.000000, 4597.000000)">
													<g>
														<path
															d="M13.2299999,0.0944999314 L0.377999999,0.0944999314 C0.277729718,0.0944999314 0.181548948,0.134245331 0.11064714,0.20514714 C0.0397453317,0.276048948 -6.82204377e-08,0.372229717 -6.82204377e-08,0.472499998 L-6.82204377e-08,15.5924999 C-6.82204377e-08,15.8012636 0.169236368,15.9704999 0.377999999,15.9704999 C0.586763629,15.9704999 0.755999991,15.8012636 0.755999997,15.5924999 L0.755999997,9.16649997 L13.2299999,9.16649997 C13.3302702,9.16656018 13.426451,9.12675463 13.4973528,9.05585283 C13.5682546,8.98495102 13.608,8.88877025 13.608,8.78849997 L13.608,0.472499998 C13.608,0.372229717 13.5682546,0.276048948 13.4973528,0.20514714 C13.426451,0.134245331 13.3302702,0.0944999314 13.2299999,0.0944999314 Z M11.34,2.36249999 L11.34,4.63049998 L12.852,4.63049998 L12.852,6.89849997 L11.34,6.89849997 L11.34,8.41049997 L9.07199997,8.41049997 L9.07199997,6.89849997 L6.80399997,6.89849997 L6.80399997,8.41049997 L4.53599998,8.41049997 L4.53599998,6.89849997 L2.26799999,6.89849997 L2.26799999,8.41049997 L0.755999997,8.41049997 L0.755999997,6.89849997 L2.26799999,6.89849997 L2.26799999,4.63049998 L0.755999997,4.63049998 L0.755999997,2.36249999 L2.26799999,2.36249999 L2.26799999,0.850499997 L4.53599998,0.850499997 L4.53599998,2.36249999 L6.80399997,2.36249999 L6.80399997,0.850499997 L9.07199997,0.850499997 L9.07199997,2.36249999 L11.34,2.36249999 L11.34,0.850499997 L12.852,0.850499997 L12.852,2.36249999 L11.34,2.36249999 Z"
															id="Shape"></path>
														<rect id="Rectangle" x="2.26799999" y="2.36249999" width="2.26799999" height="2.26799999">
														</rect>
														<rect id="Rectangle" x="4.53599998" y="4.63049998" width="2.26799999" height="2.26799999">
														</rect>
														<rect id="Rectangle" x="6.80399997" y="2.36249999" width="2.26799999" height="2.26799999">
														</rect>
														<rect id="Rectangle" x="9.07199997" y="4.63049998" width="2.26799999" height="2.26799999">
														</rect>
													</g>
												</g>
											</g>
										</g>
									</svg>
								</div>

							</footer>
							<?php
							if ( class_exists( 'amcFramework' ) ) {
								do_action( 'amc_social_share_single', $post_title, $post_permalink );
							}

							if ( has_tag() ) {
								?>
							<div class="blog-post-tag">
								<?php
								echo "<div class='tags-title'>" . esc_html__( 'Browse', 'amc' ) . '</div>';
								the_tags( ' ', ' ', ' ' );
								?>
							</div>
							<?php } ?>

							<div class="comments-blog-post-top">
								<?php
								$allowed_tag = array( 'i' => array( 'class' => array() ) );
								if ( 0 === get_comments_number( get_the_ID() ) ) {
									$css_class = ' zero-comment';
								} else {
									$css_class = '';
								} comments_popup_link( esc_html__( 'No Comment', 'amc' ), wp_kses( __( '1 Comment', 'amc' ), $allowed_tag ), wp_kses( __( 'Show Comments (%)', 'amc' ), $allowed_tag ), 'smooth' . $css_class . '', esc_attr__( 'Comments are off this post!', 'amc' ) );
								?>
							</div>

							<div class="comments-inner-page">
								<?php comments_template(); ?>
							</div>

						</div>
					</div>
				</div>

				<aside id="secondary" class="col-lg-3 col-sm-3 sidebar-main widget-area sticky" role="complementary">
					<?php
					if ( is_active_sidebar( 'right-sidebar' ) ) {
						if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'right-sidebar' ) ) :
					endif;
					}
					?>
				</aside>

			</div>

		</div>
	</div>
</div>
<div class="single-related-box">
	<div class="daymag-wrapper">
		<div class="related-column-left">
			<div class="title-bar">
				<h5><span><?php echo esc_html__( 'MORE FROM', 'amc' ); ?></span>
					<?php
						$categories = get_the_category( get_the_ID() );
						$latest_cat = count( $categories ) - 1;
					foreach ( $categories as $category ) {
						echo esc_attr( $category->cat_name ) . ( $categories[ $latest_cat ]->term_id !== $category->term_id ? ', ' : '' );
					}
					?>
				</h5>
			</div>
			<div class="modern-list-wrapper">
				<?php
				$cats          = wp_get_post_categories( $post->ID );
				$args          = array(
					'category__in'        => $cats,
					'post__not_in'        => array( $post->ID ),
					'posts_per_page'      => 3,
					'ignore_sticky_posts' => 1,
				);
				$related_query = new wp_query( $args );
				$totalrelated  = $related_query->found_posts;
				while ( $related_query->have_posts() ) {
					$related_query->the_post();
					$eachpost_id = get_the_ID();
					$post_link   = get_the_permalink( $eachpost_id );
					$post_title  = get_the_title( $eachpost_id );
					?>
				<div class="modern-list-post-box">
					<div class="modern-list-share">
						<a class="facebook"
							href="http://www.facebook.com/sharer.php?u=<?php echo urlencode( esc_url( $post_link ) ); ?>"
							onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
							target="_blank" title="Share on Facebook"><i class="mag-icon-facebook-1"></i></a>
						<a class="twitter"
							href="https://twitter.com/share?text=<?php echo htmlspecialchars( urlencode( html_entity_decode( $post_title, ENT_COMPAT, 'UTF-8' ) ), ENT_COMPAT, 'UTF-8' ); ?>&url=<?php echo urlencode( esc_url( $post_link ) ); ?>"
							onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
							target="_blank" title="Share on Twitter"><i class="mag-icon-twitter"></i></a>
						<a class="pinterest"
							href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode( esc_url( $post_link ) ); ?>&description=<?php echo htmlspecialchars( urlencode( html_entity_decode( $post_title, ENT_COMPAT, 'UTF-8' ) ), ENT_COMPAT, 'UTF-8' ); ?>&summary=&source="
							onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
							target="_blank" title="Share on Pinterest"><i class="mag-icon-pinterest"></i></a>
					</div>
					<div class="modern-list-image"><a
							href="<?php echo esc_attr( $post_link ); ?>"><?php echo amc_post_thumbnail( $each_post_id, 'amc-modern-list', '' ); ?></a>
					</div>
					<div class="modern-list-text">
						<?php if ( has_category( '', $each_post_id ) ) { ?>
						<div class="modern-list-categories">
							<?php
							$categories = get_the_category( $each_post_id );
							$latest_cat = count( $categories ) - 1;
							foreach ( $categories as $category ) {
								?>
							<a class="post-categories-link"
								href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>"><?php echo esc_attr( $category->cat_name ) . ( $categories[ $latest_cat ]->term_id !== $category->term_id ? ', ' : '' ); ?></a>
							<?php } ?>
						</div>
						<?php } ?>
						<h3><a href="<?php echo esc_url( $post_link ); ?>"><?php echo esc_attr( $post_title ); ?></a></h3>
						<div class="post-elements">
							<?php echo __( ' by ', 'amc' ); ?><a class="author-name"
								href="<?php echo get_author_posts_url( get_post_field( 'post_author', $each_post_id ) ); ?>"
								title="Posts"
								rel="author"><?php echo get_the_author_meta( 'display_name', get_post_field( 'post_author', $each_post_id ) ); ?></a>

						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="related-column-right">
			<div class="title-bar">
				<h5><?php echo esc_html__( 'LATEST', 'amc' ); ?></h5>
			</div>
			<div class="thumbnail-list-wrapper">
				<?php
				$args      = array(
					'posts_per_page'      => 6,
					'ignore_sticky_posts' => 1,
				);
				$the_query = new wp_query( $args );
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$each_post_id = get_the_ID();
					$post_link    = get_the_permalink( $each_post_id );
					$post_title   = get_the_title( $each_post_id );
					?>
				<div class="thumbnail-list-post-box">
					<div class="thumbnail-list-image"><a
							href="<?php echo esc_url( $post_link ); ?>"><?php echo amc_post_thumbnail( $each_post_id, 'amc-thumbnail-list', '' ); ?></a>
					</div>
					<div class="thumbnail-list-text">
						<h6><a href="<?php echo esc_url( $post_link ); ?>"><?php echo esc_attr( $post_title ); ?></a></h6>
						<a class="content-date"
							href="<?php echo esc_url( $post_link ); ?>"><?php echo get_the_date( 'F j', $each_post_id ); ?></a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();